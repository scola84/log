'use strict';

class Time {
  constructor() {
    this.id = null;
    this.name = null;
    this.begin = null;
    this.end = null;
  }

  getId() {
    return this.id;
  }

  setId(id) {
    this.id = id;
    return this;
  }

  getName() {
    return this.name;
  }

  setName(name) {
    this.name = name;
    return this;
  }

  start() {
    this.begin = Date.now();
    return this;
  }

  stop() {
    this.end = Date.now();
    return this;
  }

  getValue() {
    return this.end - this.begin;
  }
}

module.exports = Time;
