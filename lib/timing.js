'use strict';

class Timing {
  constructor(time) {
    this.timeProvider = time;
    this.timer = null;
    this.name = null;

    this.idHandler = null;
    this.startName = null;
    this.startHandler = null;
    this.stopName = null;
    this.stopHandler = null;

    this.times = new Map();
  }

  getTimer() {
    return this.timer;
  }

  setTimer(timer) {
    this.timer = timer;
    return this;
  }

  getName() {
    return this.name;
  }

  setName(name) {
    this.name = name;
    return this;
  }

  start(name, handler) {
    this.startName = name;
    this.startHandler = handler;

    return this;
  }

  stop(name, handler) {
    this.stopName = name;
    this.stopHandler = handler;

    return this;
  }

  id(handler) {
    this.idHandler = handler;
    return this;
  }

  handle(instance, method, ...info) {
    try {
      const name = [
        instance.constructor.name,
        method
      ].join('.');

      if (name === this.startName) {
        this.handleStart(info);
      } else if (name === this.stopName) {
        this.handleStop(info);
      }
    } catch (error) {
      console.error(error.stack);
    }
  }

  handleStart(info) {
    const key = this.startHandler(...info);

    if (key) {
      const time = this.timeProvider
        .get()
        .setName(this.name)
        .start();

      this.times.set(key, time);
    }
  }

  handleStop(info) {
    const key = this.stopHandler(...info);

    if (key) {
      const time = this.times.get(key);

      time
        .stop()
        .setId(this.idHandler(...info));

      this.timer.handleStop(time);
      this.times.delete(key);
    }
  }
}

module.exports = Timing;
