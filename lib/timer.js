'use strict';

const EventHandler = require('@scola/events');

class Timer extends EventHandler {
  constructor(timing) {
    super();

    this.timingProvider = timing;
    this.name = null;
    this.loggers = [];
    this.sources = [];
    this.timings = [];
  }

  getName() {
    return this.name;
  }

  setName(name) {
    this.name = name;
    return this;
  }

  logger(logger) {
    this.loggers.push(logger);
    return this;
  }

  source(source) {
    this.sources.push(source);
    this.bindListeners(source);

    return this;
  }

  stop() {
    this.sources.forEach((source) => {
      this.unbindListeners(source);
    });
  }

  bindListeners(source) {
    this.bindListener('debug', source, this.handleDebug);
  }

  unbindListeners(source) {
    this.unbindListener('debug', source, this.handleDebug);
  }

  start(name, handler) {
    const timing = this.timingProvider
      .get()
      .setTimer(this)
      .setName(this.name)
      .start(name, handler);

    this.timings.push(timing);
    return timing;
  }

  handleDebug(...args) {
    this.timings.forEach((timing) => {
      timing.handle(...args);
    });
  }

  handleStop(time) {
    this.loggers.forEach((logger) => {
      logger.log('timer', time);
    });
  }
}

module.exports = Timer;
