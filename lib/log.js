'use strict';

const EventHandler = require('@scola/events');

class Log extends EventHandler {
  constructor(timer) {
    super();

    this.timerProvider = timer;
    this.loggers = [];
    this.sources = [];
  }

  stop() {
    this.sources.forEach((definition) => {
      this.unbindListeners(definition.source, definition.options);
    });

    return this;
  }

  logger(logger) {
    this.loggers.push(logger);
    return this;
  }

  source(source, options) {
    options = Object.assign({
      events: [],
      info: [],
      name: ''
    }, options);

    this.sources.push({
      source,
      options
    });

    this.bindListeners(source, options);

    return this;
  }

  timer(name) {
    return this.timerProvider
      .get()
      .setName(name);
  }

  bindListeners(source, options) {
    options.events.forEach((event) => {
      this.bindListener(event, source, this.handle, event, options.name);
    });
  }

  unbindListeners(source, options) {
    options.events.forEach((event) => {
      this.unbindListener(event, source, this.handle);
    });
  }

  handle(event, ...parameters) {
    try {
      this.loggers.forEach((logger) => {
        logger.log(event, ...parameters);
      });
    } catch (error) {
      console.error(error.stack);
    }
  }
}

module.exports = Log;
