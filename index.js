'use strict';

const DI = require('@scola/di');

const Log = require('./lib/log');
const Time = require('./lib/time');
const Timer = require('./lib/timer');
const Timing = require('./lib/timing');

class Module extends DI.Module {
  configure() {
    this.inject(Log).with(
      this.provider(Timer)
    );

    this.inject(Timer).with(
      this.provider(Timing)
    );

    this.inject(Timing).with(
      this.provider(Time)
    );
  }
}

module.exports = {
  Log,
  Module
};
